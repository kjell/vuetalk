var gallery = new Vue({
	el: '#gallery',
	data: {
		gallery: {},
		heading: 'demo gallery!',
	},
	created: function () {
		var self = this;
		$.ajax({
			url: '/api/v1/images',
			dataType: "json",
			success: function (response) {
				self.gallery = response
			}
		})
	},
	methods: {
		remove: function (id) {
			console.log("About to delete")
			Vue.delete(this.gallery, id)
			console.log("done")
		}
	}
})



