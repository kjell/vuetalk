Vue.component('thumbnail', {
	props: ['image'],
	template: '#imgtmpl',
	methods: {
		star: function () {
			this.image.stars++;
			this.save();
		},
		save: function () {
			$.post('/api/v1/image/' + this.image.id + '/star', function (r) {
			})
		},

		remove: function () {
			var self = this;
			console.log("Delete " + this.image.id);
			$.post('/api/v1/image/' + this.image.id + '/delete', function (r) {
				gallery.remove(self.image.id)
			})
		}
	}

})
