package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var fOpen = false

var data map[int]*Image

func main() {
	data = map[int]*Image{
		0: &Image{0, "tree.jpg", 0},
		1: &Image{1, "dog.jpg", 0},
		2: &Image{2, "cat.jpg", 0},
	}

	r := mux.NewRouter()

	//api
	api := r.PathPrefix("/api/v1/").Subrouter()
	api.HandleFunc("/images", GetImagesHandler).Methods("GET")
	api.HandleFunc("/image/{id}/star", StarImageHandler).Methods("POST")
	api.HandleFunc("/image/{id}/delete", DeleteImageHandler).Methods("POST")
	//catch all
	r.PathPrefix("/").Handler(http.FileServer(http.Dir(".")))

	http.Handle("/", r)
	/*	if fOpen {
		open.Start("http://localhost:8080")
	}*/
	log.Println("Server starting on port 8080")
	log.Fatal(http.ListenAndServe(":8080", r))

}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "simple.html")
}

// Image contain metadata for an image
type Image struct {
	ID       int    `json:"id"`
	FileName string `json:"filename"`
	Stars    int    `json:"stars"`
}

func (i *Image) star() {
	i.Stars++
}

// StarImageHandler register a thunbs up on image and saves it
func StarImageHandler(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		log.Fatal(err)
	}
	if id < 0 {
		return
	}
	data[id].star()
	log.Println("ok", id)
	w.Write([]byte("Image starred!"))
}

// DeleteImageHandler deletes image with given id
func DeleteImageHandler(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		log.Fatal(err)
	}
	delete(data, id)
	log.Println("deleted ", id)
	w.Write([]byte("Image deleted!"))
}

// GetImagesHandler serves a datastructure which represent
// an album.
func GetImagesHandler(w http.ResponseWriter, r *http.Request) {

	b, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	w.Write(b)
}
