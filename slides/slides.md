# Vue.js


---

## Historie

- Evan You utviklet etter å ha jobbet for Google med AngularJS i flere prosjekt. <!-- .element: class="fragment" data-fragment-index="1" -->
- Community, team <!-- .element: class="fragment" data-fragment-index="2" -->
- Angular(google) vs. React (fb) vs. Vue <!-- .element: class="fragment" data-fragment-index="3" -->
- Alibaba, Baidu, Expedia, Nintendo, GitLab <!-- .element: class="fragment" data-fragment-index="4" -->

---

* 1. release feb 2014 (v0.6)
* v1.0 okt. 2015
* current v2.5.16
* 3.0 på vei.

---
## Browsere

- v2 (ie9,10,11) + resten 
- -> v3.0 ( dropper ie )

---

- vue.js - 8200 kodelinjer, 1280 linjer kommentar, 1000 blanke linjer.
- vue.min.js - 85K.
- angular.min.js: 166K
- jquery.min.js: 84K
- marko ca 30K

---

![](MVVM.png)

---

![](components.png)

---

![](lifecycle.png)
<!-- https://vuejs.org/v2/guide/instance.html -->


---

# drop in

---

## Hello World

```
    <div id="hello-world-app">
      <h1>{{ msg }}</h1>
    </div>

    <script>
      new Vue({
        el: "#hello-world-app",
        data() {
          return {
            msg: "Hello World!"
          }
        }
      });
    </script>

```

---

## Hello World2

```
    <div id="hello-world-app" v-on:click="i++">
      <h1>{{ msg }} {{i}}</h1>
    </div>

    <script>
      new Vue({
        el: "#hello-world-app",
        data() {
          return {
            msg: "Hello World!",
            i : 0,
          }
        }
      });
    </script>

```

---

## Component

```
Vue.component('button-counter', {
    props:['msg'],
  data: function () {
    return {
      count: 0
    }
  },
  template: `<button v-on:click="count++">{{ msg }}
   {{ count }} times.</button>`
})
```

---

```
<body>
  <div id="demoapp">
    <h1>{{heading}}</h1>
    <button-counter v-bind:msg="'hello'"></button-counter>
  </div>
</body>
```

---
 * vuex
 * statemanagement (redux, flux)

---

## Ressurser

 * https://www.vuemastery.com/conferences/vueconf-2018/how-we-do-vue-at-gitLab-jacob-schatz/
 * http://alex.amiran.it/post/2017-04-09-using-vue-js-without-webpack.html
 * https://vuejs.org/v2/guide/
 * https://vuejs.org/v2/cookbook/
---

## Demo1
<!-- Dropin uten tools-->
---

## Demo2
<!-- med tools-->

---

## Demo3
